<?php
session_start();
					
if(isset($_SESSION["user"]))
{
						
}
	else
{
	header("location:index.php");
}
?>
<!DOCTYPE HTML>
<html>

<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link rel="stylesheet" href="bootstrap-3.3.7/dist/css/bootstrap.min.css">
    <script src="js/holder/holder.js"></script>
    <script src="bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/estiloranking.css">
    <link rel="stylesheet" href="css/css_barra.css">
    <link rel="stylesheet" href="css/barra.css">
    <script src="angular.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi">


    </script>
    <script type="text/javascript" src="https://www.google.com/jsapi?autoload={
        'modules':[{
        'name':'visualization',
        'version':'1',
        'packages':['corechart']
        }]
        }"></script>
</head>

<body ng-app="myapp" ng-controller="controlador" ng-init="init()">
    <?php 
            include("header.php"); 
        
            include("barralateral.php");
        ?>

    <!----------cuerpo de pagina ----------->

    <div>
        <div class="container text-center">

            <select id="tiempo" class="form-control col-sm-4" ng-change="init()" ng-model="masc">
                <option value="1" selected="selected">Ranking diario</option>
                <option value="2">Ranking semanal</option>
                <option value="3">Ranking historico</option>
            </select>

            <div id="contrank" class="row col-md-12">

                <div class="trank col-sm-12 col-md-3 text-center">
                    <h2>Ranking "Me gusta"</h2>
                    <img src="img/icono.png" class="img-fluid" />
                    <table class="table">
                        <caption>Ranking "Me gusta"</caption>
                        <thead>
                            <tr>
                                <th>Pos.</th>
                                <th>Nombre</th>
                                <th>Puntuaci&oacute;n</th>
                            </tr>
                        </thead>
                        <tbody ng-repeat="masc in puestoslikes track by $index">
                            <tr>
                                <td>{{$index +1}}</td>
                                <td><a href="perfil_mascota.php?idmasc={{masc.id}}">{{masc.nombre}}</a></td>
                                <td>{{masc.cant}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="trank col-sm-12 col-md-6 text-center">
                    <img src="img/crown.png" class="img-fluid" id="crown" />
                    <h2>Ranking general</h2>
                    <img src="img/icono.png" class="img-fluid" />
                    <table class="table">
                        <caption>Ranking general</caption>
                        <thead>
                            <tr>
                                <th>Pos.</th>
                                <th>Nombre</th>
                                <th>Puntuaci&oacute;n</th>
                            </tr>
                        </thead>
                        <tbody ng-repeat="masc in puestosgen track by $index">
                            <tr>
                                <td>{{$index +1}}</td>
                                <td><a href="perfil_mascota.php?idmasc={{masc.id}}">{{masc.nombre}}</a></td>
                                <td>{{masc.cant}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="trank col-sm-12 col-md-3 text-center">
                    <h2>Ranking seguidores</h2>
                    <img src="img/icono.png" class="img-fluid" />
                    <table class="table">
                        <caption>Ranking de seguidores</caption>
                        <thead>
                            <tr>
                                <th>Pos.</th>
                                <th>Nombre</th>
                                <th>Puntuaci&oacute;n</th>
                            </tr>
                        </thead>
                        <tbody ng-repeat="masc in puestosseg track by $index">
                            <tr>
                                <td>{{$index +1}}</td>
                                <td><a href="perfil_mascota.php?idmasc={{masc.id}}">{{masc.nombre}}</a></td>
                                <td>{{masc.cant}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div id="grafica"></div>
                </div>
            </div>


        </div>
    </div>
    <footer class="container-fluid text-center">
        <p>Footer Text</p>
    </footer>
    <script>
        $(document).ready(function() {
            $body = $("body");

            $(document).on({
                ajaxStart: function() {
                    $body.addClass("loading");
                },
                ajaxStop: function() {
                    $body.removeClass("loading");
                }
            });
        });

    </script>
    <script>
        var app = angular.module("myapp", []);

        app.controller("controlador", function($scope, $http) {

            $scope.init = function() {

                $scope.idmasc = <?php echo $_SESSION["idmasc"]; ?>;
                $scope.tiempo = $("#tiempo").val();

                $scope.loadMascotasBarra = function() {

                    $http({
                            method: "post",
                            url: "php/load_mascotas.php",
                            cache: "false",
                            dataType: "json",
                            data: $.param({
                                'iduser': <?php echo $_SESSION['iduser']; ?>
                            }),
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).success(function(data) {

                            $scope.barramascotas = data;

                        })

                        .error(function(error, status) {
                            $scope.data.error = {
                                message: error,
                                status: status
                            };
                            console.log($scope.data.error.status);
                            alert($scope.data.error);

                        });

                }

                //EMPIEZA RANKLIKES
                $scope.loadRankLikes = function(idmasc, tiempo) {
                    $http({
                            method: "post",
                            url: "php/load_ranklikes.php",
                            cache: "false",
                            dataType: "json",
                            data: {
                                "idmasc": $scope.idmasc,
                                "tiempo": $scope.tiempo
                            },
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).success(function(data) {

                            $scope.puestoslikes = data;


                        })

                        .error(function(error, status) {
                            $scope.data.error = {
                                message: error,
                                status: status
                            };
                            console.log($scope.data.error.status);
                            alert($scope.data.error);

                        });

                }
                //CIERRA RANKLIKES
                //EMPIEZA RANKGENERAL
                $scope.loadRankGeneral = function(idmasc, tiempo) {

                    $http({
                            method: "post",
                            url: "php/load_rankgeneral.php",
                            cache: "false",
                            dataType: "json",
                            data: {
                                "idmasc": $scope.idmasc,
                                "tiempo": $scope.tiempo
                            },
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).success(function(data) {

                            $scope.puestosgen = data;
                        
                            // INICIA GOOGLE CHARTS
                            /*Se carga el modulo que se va a utilizar en este caso visualization,
                            seguido de la versión y el paquete que ses corechart para la grafica*/
                            google.load("visualization", "1", {
                                packages: ["corechart"],
                                callback: dibujarGrafico($scope.puestosgen)
                            });

                            /* En esta linea se indica que se va a llamar a la función dibujarGrafico
                            una vez que el documento se haya terminado de cargar*/
                            //google.setOnLoadCallback(dibujarGrafico);

                            function dibujarGrafico(puestos) {

                                /* La informacion que se obtuvo del servidor se convierte a un dataTable
                                   para que se muestre en el grafico */

                                // Create the data table.
                                var data = new google.visualization.DataTable();
                                data.addColumn('string', 'Mascotas');
                                data.addColumn('number', 'Ranking');


                                for (var key in puestos) {

                                    data.addRow([puestos[key].nombre, parseFloat(puestos[key].cant)]);

                                }


                                /* Se definen algunas opciones para el grafico*/
                                var opciones = {
                                    title: 'TOP 10 Mascotas',
                                    hAxis: {
                                        title: 'Mascotas',
                                        titleTextStyle: {
                                            color: 'green'
                                        }
                                    },
                                    vAxis: {
                                        title: 'Ranking',
                                        titleTextStyle: {
                                            color: 'green'
                                        }
                                    },
                                    backgroundColor: '#FFF',
                                    legend: {
                                        position: 'bottom',
                                        textStyle: {
                                            color: 'green',
                                            fontSize: 13
                                        }
                                    },
                                    width: 1200,
                                    height: 700
                                };
                                /* se crea un objeto de la clase google.visualization.ColumnChart  ( Grafica de columna )
                                 en el cual se indica cual es el elemento HTML que contendrá a la gráfica */
                                var grafico = new google.visualization.ColumnChart(document.getElementById('grafica'));
                                /* Se llama al método draw para dibujar la gráfica*/
                                grafico.draw(data, opciones);
                            }

                        })

                        .error(function(error, status) {
                            $scope.data.error = {
                                message: error,
                                status: status
                            };
                            console.log($scope.data.error.status);
                            alert($scope.data.error);

                        });

                }
                //CIERRA RANKGENERAL
                //EMPIEZA RANKSEGUIDORES
                $scope.loadRankSeg = function(idmasc, tiempo) {


                    $http({
                            method: "post",
                            url: "php/load_rankseg.php",
                            cache: "false",
                            dataType: "json",
                            data: {
                                "idmasc": $scope.idmasc,
                                "tiempo": $scope.tiempo
                            },
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).success(function(data) {

                            $scope.puestosseg = data;



                        })

                        .error(function(error, status) {
                            $scope.data.error = {
                                message: error,
                                status: status
                            };
                            console.log($scope.data.error.status);
                            alert($scope.data.error);

                        });

                }
                //CIERRA RANKSEGUIDORES




                $scope.loadMascotasBarra();
                $scope.loadRankLikes();
                $scope.loadRankGeneral();
                $scope.loadRankSeg();
            }
        });

    </script>
</body>

</html>
