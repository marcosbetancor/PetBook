<?php
session_start();
					
if(isset($_SESSION["user"]))
{
	$_SESSION["perfilmasc"] = $_GET["idmasc"];					
}
	else
{
	header("location:index.php");
}
?>
<!DOCTYPE HTML>
<html>

<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	
	<link rel="stylesheet" href="css/perfil_mascota.css">
	
    <link rel="stylesheet" href="bootstrap-3.3.7/dist/css/bootstrap.min.css">
    
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
	<script src="js/holder/holder.js"></script>
	
	<script src="bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/boton.js"></script>
	<link rel="stylesheet" type="text/css" href="css/barra.css">
	<link rel="stylesheet" href="css/css_barra.css">
	<script src="angular.min.js"></script>
	
</head>

<body ng-app="myapp" ng-controller="controlador" ng-init="init()">
    <?php 
        include("header.php"); 
        
        include("barralateral.php");
    ?>


	<!---cuerpo de pagina-->


	<div>
	<div class="col-lg-12 col-sm-6">
		<div class="card hovercard">
			<div class="card-background">
				<img class="card-bkimg" alt="" src="img/perfil_mascotas.jpg">
				<!-- http://lorempixel.com/850/280/people/9/ -->
			</div>
			<div class="useravatar">
			
				<img alt="" ng-src="{{masc.imgperf}}">

			</div>
			<div class="card-info"> 
                <span class="card-title">
					{{masc.nombre}}
				</span>
				<div><button class="btn btn-primary" id="seguir" ng-click="seguirMascota()">Seguir +</button></div>

			</div>
		</div>
	</div>
	<div class="col-lg-1 col-sm-6">
	</div>
	<div class="col-lg-10 col-sm-6">
		<div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
			<div class="btn-group" role="group">
				<button type="button" id="stars" class="btn btn-success" href="#tab1" data-toggle="tab"><i class="fa fa-paw" aria-hidden="true"></i>
                <div class="hidden-xs">Mascotas</div>
            </button>
			</div>
			<div class="btn-group" role="group">
				<button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><i class="fa fa-camera" aria-hidden="true"></i>
                <div class="hidden-xs">Fotos</div>
            </button>
			</div>
			<div class="btn-group" role="group">
				<button type="button" id="following" class="btn btn-default" href="#tab3" data-toggle="tab"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <div class="hidden-xs">informacion</div>
            </button>
			</div>
		</div>

		<div class="well">
			<div class="tab-content">
				<div class="tab-pane fade in active" id="tab1">
                    <div class="provicional">
                    <a href="php/mostrar-cod-qr.php">  
                        <did class="btn btn-primary" name="nombre">
                            Generar codigo qr
                        </did>
                     </a>
                    </div>
                    
                       <div class="provicional"> 
                        <a href="pdf/generador_pdf.php">  
                        <did class="btn btn-primary" name="nombre">
                            Generar PDF
                        </did>
                        </a>
                       </div>
				
				</div>
				<div class="tab-pane fade in" id="tab2">
					<div class="panel panel-success">
						<div class="panel-heading">Galeria</div>

						<div class="row" >
							<div class="col-sm-6 col-md-3" data-ng-repeat="foto in fotos">
								<a href="#" class="thumbnail"><img ng-src="{{foto.url}}" height="300" width="215"></a>
                                {{foto.id}}
							</div>
						</div>
					</div>

				</div>
				<div class="tab-pane fade in" id="tab3">
					<!---magia here-->
					<div class="panel panel-success">
						<div class="panel-heading">editar info</div>
						<div class="panel-body">

							<div class="row">
								<div class="col-md-2">

								</div>
								<div class="col-md-8" style='border:solid 1px red;'>
									<p>
                                   Fecha de nac.: {{masc.fecnac}}<br> Sexo: {{masc.sexo}}<br> Raza: {{masc.raza}}<br> Tamaño: {{masc.tam}}<br>
                                </p>
								</div>
								<div class="col-md-2">

								</div>
							</div>


						</div>
					</div>




				</div>
			</div>
		</div>

	</div>
    </div>
       <script>
    var app = angular.module("myapp", []);
    
    app.controller("controlador", function($scope, $http) {
        
        
            $scope.init = function() {    

                
                $scope.loadMascotasBarra = function() {

                    $http({
                            method: "post",
                            url: "php/load_mascotas.php",
                            cache: "false",
                            dataType: "json",
                            data: $.param({
                                'iduser': <?php echo $_SESSION['iduser']; ?>
                            }),
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).success(function(data) {

                            $scope.barramascotas = data;
                            $scope.mascotas = data;

                        })

                        .error(function(error, status) {
                            $scope.data.error = {
                                message: error,
                                status: status
                            };
                            console.log($scope.data.error.status);
                            alert($scope.data.error);

                        });

                }
                               
				 $scope.loadMascota = function() {
                        
                        $scope.idmasc = <?php echo $_SESSION["perfilmasc"]; ?>;
                        $scope.iduser = <?php echo $_SESSION["iduser"]; ?>;
                        
                        $http({
                                method: "post",
                                url: "php/load_mascotaperfil.php",
                                cache: "false",
                                dataType: "json",
                                data: {"idmasc": $scope.idmasc, "iduser": $scope.iduser },
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                }
                            }).success(function(data) {
                            
                                $scope.masc = data;
                                $scope.fotos = $scope.masc.fotos;
								alert(JSON.stringify("MASC: " + $scope.masc));
								alert("Fotos: " + JSON.stringify($scope.fotos));
							

                            })

                            .error(function(error, status) {
                                $scope.data.error = {
                                    message: error,
                                    status: status
                                };
                                console.log($scope.data.error.status);
                                alert($scope.data.error);

                            });

                }
                  
                    $scope.loadMascotasBarra();
                    $scope.loadMascota();
                
                }
                
                $scope.seguirMascota = function(){
                
                        $scope.idmasc = <?php echo $_SESSION["perfilmasc"]; ?>;
                        $scope.idseg = <?php echo $_SESSION["idmasc"]; ?>;
                        
                        $http({
                                method: "post",
                                url: "php/seguirmascota.php",
                                cache: "false",
                                dataType: "json",
                                data: {"idmasc": $scope.idmasc, "idseg": $scope.idseg },
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                }
                            }).success(function(data) {
                            
                                if($("#seguir").hasClass("sigue")){

                                    $("#seguir").removeClass("sigue");
                                    $("#seguir").text("Seguir +")


                                } else {
                                    $("#seguir").addClass("sigue");
                                    $("#seguir").text("No seguir -");
                                }


                            })

                            .error(function(error, status) {
                                $scope.data.error = {
                                    message: error,
                                    status: status
                                };
                                console.log($scope.data.error.status);
                                alert($scope.data.error);

                            });
                    }
                
                  
                
			});
    </script>
</body>

</html>
