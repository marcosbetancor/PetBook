<!DOCTYPE html>
<html>

<head>

	<link rel="stylesheet" href="bootstrap-3.3.7/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- ****************************** SESION GOOGLE ****************************** -->
	<meta name="google-signin-scope" content="profile email">
	<meta name="google-signin-client_id" content="784515571500-795k3r0d814tg187lbe7tekcdu2kbf88.apps.googleusercontent.com">
	<!-- ****************************** SESION GOOGLE ****************************** -->


</head>

<body>
	<!-- ****************************** SESION GOOGLE ****************************** -->
	<script>
		function onSuccess(googleUser) {
			alert('Logged in as: ' + JSON.stringify(googleUser.getBasicProfile().getName()));

			document.getElementById('inputemail').value = googleUser.getBasicProfile().getEmail();
		}

		function onFailure(error) {
			alert(JSON.stringify(error));
		}

		function renderButton() {
			gapi.signin2.render('my-signin2', {
				'scope': 'profile email',
				'width': 240,
				'height': 50,
				'longtitle': true,
				'theme': 'dark',
				'onsuccess': onSuccess,
				'onfailure': onFailure
			});
		}

	</script>

	<script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
	<!-- ****************************** SESION GOOGLE ****************************** -->
	<script>
		// This is called with the results from from FB.getLoginStatus().
		function statusChangeCallback(response) {
			console.log('statusChangeCallback');
			console.log(response);
			// The response object is returned with a status field that lets the
			// app know the current login status of the person.
			// Full docs on the response object can be found in the documentation
			// for FB.getLoginStatus().
			if (response.status === 'connected') {
				// Logged into your app and Facebook.
				testAPI();
			} else {
				// The person is not logged into your app or we are unable to tell.
				document.getElementById('status').innerHTML = 'Please log ' +
					'into this app.';
			}
		}

		// This function is called when someone finishes with the Login
		// Button.  See the onlogin handler attached to it in the sample
		// code below.
		function checkLoginState() {
			FB.getLoginStatus(function(response) {
				statusChangeCallback(response);
			});
		}

		window.fbAsyncInit = function() {
			FB.init({
				appId: '{779516058875981}',
				cookie: true, // enable cookies to allow the server to access 
				// the session
				xfbml: true, // parse social plugins on this page
				version: 'v2.8' // use graph api version 2.8
			});

			// Now that we've initialized the JavaScript SDK, we call 
			// FB.getLoginStatus().  This function gets the state of the
			// person visiting this page and can return one of three states to
			// the callback you provide.  They can be:
			//
			// 1. Logged into your app ('connected')
			// 2. Logged into Facebook, but not your app ('not_authorized')
			// 3. Not logged into Facebook and can't tell if they are logged into
			//    your app or not.
			//
			// These three cases are handled in the callback function.

			FB.getLoginStatus(function(response) {
				statusChangeCallback(response);
			});

		};

		// Load the SDK asynchronously<div id="fb-root"></div>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.9&appId=779516058875981";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

		// Here we run a very simple test of the Graph API after login is
		// successful.  See statusChangeCallback() for when this call is made.
		function testAPI() {
			console.log('Welcome!  Fetching your information.... ');
			FB.api('/me', {
				fields: 'name,email'
			}, function(response) {
				console.log('Successful login for: ' + response.name);
				document.getElementById('inputemail').value = response.email;
			});
		}

	</script>

	<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->

	<div id="contenedor">

		<div id="sub_contenedor">
			<img id="imagen" src="img/logo.png" />
			<div class="container">
				<img src="img/icono.png">
				<form method="POST" action="php/login_pr.php">
					<div class="form-input">
						<i class="fa fa-user" style="font-size:24px;color:#2ECC71"></i>
						<input type="email" name="user" placeholder="Ingrese su correo electronico" id="inputemail">
					</div>
					<div class="form-input">
						<i class="fa fa-lock" style="font-size:24px;color:#2ECC71"></i>
						<input type="password" name="clave" placeholder="ingrese contrase&ntilde;a">
					</div>
					<input type="submit" value="Iniciar sesion" class="btn-login" />

				</form>

				<form method="POST" action="php/formulario.php">
					<input type="submit" value="registrarse" class="btn-login" />

					<!-- BOTON GOOGLE-->
					<div id="my-signin2" class="btn-google" style="margin-left:55px;"></div>
					<!-- BOTON GOOGLE-->
					<!-- BOTON FB -->
					<div class="fb-login-button btn-facebook" data-width="300" data-max-rows="1" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="true" data-use-continue-as="false"></div>
					<!-- BOTON FB -->
				</form>

			</div>
		</div>
		<img id="fondo" src="img/fondo.jpg">

	</div>
</body>

</html>
