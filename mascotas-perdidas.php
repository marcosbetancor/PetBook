<?php
session_start();
					
if(isset($_SESSION["user"]))
{
						
}
	else
{
	header("location:index.php");
}
?>
<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/barra.css">
    <link rel="stylesheet" href="css/css_barra.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/estiloranking.css">
    <script src="angular.min.js"></script>
    <link rel="stylesheet" href="css/css-mascotas-perdidas.css">
    
   
</head>

<body ng-app="myapp" ng-controller="controlador" ng-init="init()">
    <?php 
            include("header.php"); 
        
            include("barralateral.php");
        ?>


    <!----------cuerpo de pagina ---------->

    <div class="container">
        <div class="row">


                    <div class="panel panel-success mascota" ng-repeat="mascota in mascotas">
                        <div class="panel-heading">{{mascota.nombre}}</div>
                        <div class="panel-body">
                            
                        <img ng-src="{{mascota.imgperf}}" class="perfmasc pull-left" />
                            
                            <div class="infomascota pull-right">
                                <p>
                                    Nombre: {{mascota.nombre}}<br>
                                    Fecha de nac.: {{mascota.fecnac}}<br>
                                    Sexo: {{mascota.sexo}}<br>
                                    Raza: {{mascota.raza}}<br>
                                    Tamaño: {{mascota.tam}}<br>
                                    Busca pareja: {{mascota.buscpar|blankZero}}<br>
                                    Adopci&oacute;n: {{mascota.enadop|blankZero}}<br>
                                    Perdido: {{mascota.perdido|blankZero}}<br>
                                </p>
                                <a href="perfil_mascota.php?idmasc={{mascota.id}}" class="btn btn-primary">Perfil -></a>

                            </div>
                        </div>
                    </div>

<!--                 </div>
            </div> -->
        </div>
    </div>    

</body>

</html>

<script>
    var app = angular.module("myapp", []);
	
	app.filter('blankZero', function () {
	  return function (text) {

		if(text=="0") {
			text = "No";
		} else{
			 text= "Si";
		}
		return text;
	  };
	});
	
    app.controller("controlador", function($scope, $http) {


        $scope.init = function() {

            $scope.loadMascotasBarra = function() {

                $http({
                        method: "post",
                        url: "php/load_mascotas.php",
                        cache: "false",
                        dataType: "json",
                        data: $.param({
                            'iduser': <?php echo $_SESSION['iduser']; ?>
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    }).success(function(data) {

                        $scope.barramascotas = data;

                    })

                    .error(function(error, status) {
                        $scope.data.error = {
                            message: error,
                            status: status
                        };
                        console.log($scope.data.error.status);
                        alert($scope.data.error);

                    });

            }

                    $scope.loadMascotasInRatio = function() {

                        $http({
                                method: "post",
                                url: "php/load_mascotasbuscaperdida.php",
                                cache: "false",
                                dataType: "json",
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                }
                            }).success(function(data) {

                                $scope.mascotas = data;

                                $scope.posuser = '<?php include("php/posenmapa.php"); ?>';

                                //SEPARO LAS COORDENADAS EN LAT Y LONG.
                                $scope.posuser = $scope.posuser.split(",");
                                // CREO EL OBJETO DE LA POSICION DEL USUARIO, PERO PRIMERO PARSEO A FLOAT LAT Y LON SI NO NO ANDA
                                var lluser = new google.maps.LatLng(parseFloat($scope.posuser[0]), parseFloat($scope.posuser[1]));
                                var llmasc;
                                var lat;
                                var lon;
                                var cad;
                                var dist;
                                angular.forEach($scope.mascotas, function(value, key) {
                                    
                                    //SEPARO LAS COORDENAS EN LAT Y LONG POR CADA MASCOTA
                                    var cad = value.pos.split(",");
                                    lat = cad[0];
                                    lon = cad[1];
                                    
                                    
                                    // CREO EL OBJETO DE LA POSICION DE LA MASCOTA, PERO PRIMERO PARSEO A FLOAT LAT Y LON SI NO NO ANDA
                                    llmasc = new google.maps.LatLng(parseFloat(lat), parseFloat(lon));
                                    //COMPUTEDISTANCEBETWEEN DEVUELVE LA DIFERENCIA EN METROS, LO PASO A KM DIVIDIENDO POR 1000
                                    dist = (google.maps.geometry.spherical.computeDistanceBetween(lluser, llmasc)) / 1000;
                                    
                                    //INSERT LA DISTANCIA EN CADA MASCOTA.
                                    value.dist = dist;
                                });

                            })

                            .error(function(error, status) {
                                $scope.data.error = {
                                    message: error,
                                    status: status
                                };
                                console.log($scope.data.error.status);
                                alert($scope.data.error);

                            });

                    }

                $scope.buscarPor = function(prop, val, raza) {


                    return function(item) {

                        var idsexo = $("#mimasc").find(':selected').data('idsexo');
                        var tam = $("#tam").val();

                        if (item[prop] < val && item['idraza'] == raza && item['idtam'] == tam && item['idsexo'] != idsexo) {
                            
                            return true;

                        } else {
                            
                            return false;
                        }
                    }
                }

            $scope.loadTipo = function() {
                $http.get("php/load_tipo.php")
                    .success(function(data) {
                        $scope.tipomasc2 = data;
                    })
            }
                
            $scope.loadRaza = function() {

                $http({
                        method: "post",
                        url: "php/load_raza.php",
                        cache: "false",
                        dataType: "json",
                        data: $.param({
                            'idtipo': $scope.tipomasc
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    }).success(function(data) {

                        $scope.raza2 = data;
                    })

                    .error(function(error, status) {
                        $scope.data.error = {
                            message: error,
                            status: status
                        };
                        console.log($scope.data.error.status);
                        alert($scope.data.error);
                        alert(error);
                        alert(status);
                    })

                    .then(function(xhr, textStatus) {
                        //alert(xhr.status);
                        //console.log(xhr.status);
                    });

            }
            
            $scope.loadMascotasBarra();
            $scope.loadMascotasInRatio();
            $scope.loadTipo();
            $scope.loadRaza();

        }
    });

</script>