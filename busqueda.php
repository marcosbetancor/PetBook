<?php
session_start();
					
if(isset($_SESSION["user"]))
{
    $_SESSION["buscar"] = $_GET["buscar"];
}
	else
{
	header("location:index.php");
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link rel="stylesheet" href="bootstrap-3.3.7/dist/css/bootstrap.min.css">
    <script src="js/holder/holder.js"></script>
   <script src="bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/estiloranking.css">
    <link rel="stylesheet" href="css/css_barra.css">
    <link rel="stylesheet" href="css/barra.css">
    <link rel="stylesheet" href="css/busqueda.css">
    <script src="angular.min.js"></script>
</head>

<body ng-app="myapp" ng-controller="controlador" ng-init="init()">
    
    <?php 
        include("header.php"); 
        
        include("barralateral.php");
    ?>
    <div class="row">
    
    <div class="col-md-3 col-xs-3"> <div class="laterales"></div></div>
    <div class="col-md-6">
    <div class="container" ng-repeat="masc in mascotas">
       
           <div class="panel panel-default ">
              <!-- Default panel contents -->
              <div class="panel-heading col-sm-12"><a ng-href="perfil_mascota.php?idmasc={{masc.id}}">{{masc.nombre}}</a></div>
              <div class="panel-body">
               
                  <img ng-src="{{masc.imgperf}}" class="img-responsive imgperf" >
                  
                  
                  <button class="btn btn-primary btn-estilo" ng-click="seguirMascota(masc.id, $event)" data-idmasc="{{masc.id}}" ng-if="masc.seg == true">No seguir -</button>
                <button class="btn btn-primary btn-estilo" ng-click="seguirMascota(masc.id, $event)" data-idmasc="{{masc.id}}" ng-if="masc.seg == false">Seguir +</button>
              </div>
            </div>

    </div>
    </div>

     <div class="col-md-3"> <div class="laterales"></div></div>
    
    </div>
    
    <script>
    var app = angular.module("myapp", []);
    app.controller("controlador", function($scope, $http) {


        $scope.init = function() {

            $scope.loadMascotasBarra = function() {

                $http({
                        method: "post",
                        url: "php/load_mascotas.php",
                        cache: "false",
                        dataType: "json",
                        data: $.param({
                            'iduser': <?php echo $_SESSION['iduser']; ?>
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    }).success(function(data) {

                        $scope.barramascotas = data;

                    })

                    .error(function(error, status) {
                        $scope.data.error = {
                            message: error,
                            status: status
                        };
                        console.log($scope.data.error.status);
                        alert($scope.data.error);

                    });     
            }
                      
            $scope.buscarMascota = function() {

               var buscar = "<?php echo $_GET["buscar"]; ?>";
               var idmasc = <?php echo $_SESSION["idmasc"]; ?>
              
                $http({
                        method: "post",
                        url: "php/buscarmascota.php",
                        cache: "false",
                        dataType: "json",
                        data: {"buscar": buscar, "idmasc": idmasc },
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    }).success(function(data) {

                        $scope.mascotas = data;
                        alert($scope.mascotas);
                    
                    })

                    .error(function(error, status) {
                        $scope.data.error = {
                            message: error,
                            status: status
                        };
                        console.log($scope.data.error.status);
                        alert($scope.data.error);

                    });

                    
                    
            }
                      

            $scope.loadMascotasBarra();
            $scope.buscarMascota();
        }
            
            $scope.seguirMascota = function(id, e){
                        
                        $scope.el = e.target;
                        $scope.idmasc = id;
                        $scope.idseg = <?php echo $_SESSION["idmasc"]; ?>;
                        
                        $http({
                                method: "post",
                                url: "php/seguirmascota.php",
                                cache: "false",
                                dataType: "json",
                                data: {"idmasc": $scope.idmasc, "idseg": $scope.idseg },
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                }
                            }).success(function(data) {
                            
                                if($($scope.el).hasClass("sigue")){

                                    $($scope.el).removeClass("sigue");
                                    $($scope.el).text("Seguir +")


                                } else {
                                    $($scope.el).addClass("sigue");
                                    $($scope.el).text("No seguir -");
                                }


                            })

                            .error(function(error, status) {
                                $scope.data.error = {
                                    message: error,
                                    status: status
                                };
                                console.log($scope.data.error.status);
                                alert($scope.data.error);

                            });
                    }
    });

</script>
</body>

</html>
