<!-- *************************************** EMPIEZA HEADER *************************************** -->
<header>
	<nav class="navbar navbar-inverse" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>                        
                          </button>
				<a class="navbar-brand" href="#"><img alt="Logo de PetBook" id="logo" src="img/logo.png" /></a>
			</div>
			<div>
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">

						<li><a href="perfil.php">Mi perfil</a></li>
						<li><a href="ranking.php">Ranking</a></li>
						<li><a href="mascotas-perdidas.php">Mascotas perdidas</a></li>
						<li><a href="buscar_pareja.php">Buscar pareja</a></li>

					</ul>

					<form class="navbar-form navbar-left" action="busqueda.php" method="get">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Buscar" ng-model="inputbuscar" name="buscar">
						</div>
						<button type="submit" class="btn btn-default">
                          <img id="lupa"src="img/lupa.png"/>
                         </button>
					</form>



<div class="ajustar_noti">

					<div class="btn-group btn_notificaciones" class="">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<div class="notificaciones">
									<i class="fa fa-group" style="font-size:24px"></i>
								</div>
							</a>
							<ul class="dropdown-menu pull-right">
								<li><a href="#">solicitud1</a></li>
								<li><a href="#">solicitud2</a></li>
							</ul>
						</li>
					</div>




					<div class="btn-group btn_notificaciones" class="">
						
                        <li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">

								<div class="notificaciones">
									<i class="fa fa-bell" style="font-size:24px"></i>

								</div>
							</a>
							<ul class="dropdown-menu pull-right">
								<li><a href="#">noti1</a></li>

								<li><a href="#">noti2</a></li>
							</ul>
                            </li>
                       
					</div>



					<div class="btn-group btn_notificaciones" class="">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<div class="notificaciones">
									<?php
					
                                        require("php/elegir_imagen_usuario.php");
                                        if(isset($_SESSION["user"]))
                                        {
                                            echo $_SESSION["user"];
                                        }
                                            else
                                        {
                                            header("location:index.php");
                                        }
                                        ?>
										<img src="img/<?php echo $img;?>" id="user_foto" class="img-circle" />
								</div>
							</a>
							<ul class="dropdown-menu pull-left">
								<li><a href="#">Configuracion</a></li>
								<li class="divider"></li>
								<li><a href="logout.php">Salir</a></li>
                            </ul>
						</li>
						
					</div>
                </div>
				</div>
			</div>
	</nav>
</header>
<!-- *************************************** TERMINA HEADER *************************************** -->
