<?php
session_start();
					
if(isset($_SESSION["user"]))
{
   
}
	else
{
	header("location:index.php");
}
?>
    <!DOCTYPE HTML>
    <html>

    <head>
        <link rel="stylesheet" href="bootstrap-3.3.7/dist/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/estiloPareja.css">
        <link rel="stylesheet" href="css/css_barra.css">
        <link rel="stylesheet" type="text/css" href="css/barra.css">
        <script src="angular.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuylZC7LDwp8B7q8wB4lXQPGHvD1f7TZQ"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false&libraries=geometry"></script>
    </head>

    <body ng-app="myapp" ng-controller="controlador" ng-init="init()">
        <?php 
        include("header.php"); 
        
        include("barralateral.php");
    
    ?>

        <!----------cuerpo de pagina ---------->

        <div class="container">
            <!-- ************************** EMPIEZAN SELECTS ************************************************ -->
            <div class="row">
                <div class="col-md-3">

                    <label for="mimascota">Mi mascota:</label>
                    <select id="mimasc" class="form-control" ng-model="mimasc" name="mimascota" ng-change="loadRaza()">
                    <option value="{{masc.id}}" data-idtipo="{{masc.tipomasc}}" data-idsexo="{{masc.idsexo}}" ng-repeat="masc in barramascotas" >{{masc.nombre}}</option>
				</select>
                </div>
            

                <div class="col-md-3">

                    <label for="Iddist">Distancia:</label>
                    <select class="form-control" id="distancia" ng-model="disteleg" name="Iddist">
                    <option value="5" selected="selected"> < 5 KM</option>
                    <option value="10"> &lt; 10 KM</option>
                    <option value="20"> &lt; 20 KM</option>
                    <option value="50"> &lt; 50 KM</option>
                    <option value="100"> &lt; 100 KM</option>
				</select>
                </div>
               
                
                <div class="col-md-3">
                    <label for="IdRaza">Raza:</label>
                    <select class="form-control" id="IdRaza" name="IdRaza" title="Raza/Especie" ng-model="razaeleg">
                    	<option value="{{raza.id}}" ng-repeat="raza in raza2">{{raza.descrip}}</option>
                    </select>
                    </div>
                
                   <div class="col-md-3">

                    <label for="tam">Tamaño:</label>
                    <select class="form-control" id="tam" ng-model="tameleg" name="tam">
                        <option value="1" selected="selected">Pequeño</option>
                        <option value="2">Mediano</option>
                        <option value="3">Grande</option>
				    </select>
                </div>
            </div>
        </div>

        <!-- ************************** TERMINA SELECTS ************************************************ -->
        <!-- ************************** EMPIEZA ESTATICO *********************************************** -->
        <img class="img-responsive pata" src="img/pata.gif">

        <!-- ************************* TERMINA ESTATICO *********************************************** -->

        <!-- ************************** CONTENEDOR MASCOTAS ************************************************ -->

        <div class="panel panel-success mascota" ng-repeat="mascota in mascotas | filter: buscarPor('dist', disteleg, razaeleg, mimasc)">
            <div class="panel-heading">{{mascota.nombre}}</div>
            <div class="panel-body">
                <img ng-src="{{mascota.imgperf}}" class="perfmasc pull-left" />
                <div class="infomascota pull-right">
                    <p>
                        Nombre: {{mascota.nombre}}<br> Fecha de nac.: {{mascota.fecnac}}<br> Sexo: {{mascota.sexo}}<br> Raza: {{mascota.raza}}<br> Tamaño: {{mascota.tam}}<br> Busca pareja: {{mascota.buscpar}}<br> Adopci&oacute;n: {{mascota.enadop}}<br> Perdido: {{mascota.perdido}}<br> Distancia: {{mascota.dist}}<br>
                    </p>
                    <a href="" class="btn btn-danger" ng-click="insertaCita(mimasc, mascota.id)"><span class="glyphicon glyphicon-heart"></span></a>

                </div>
            </div>
        </div>

        <!-- ************************** TERMINA CONTENEDOR MASCOTAS ************************************************ -->


        <script>
            var app = angular.module("myapp", []);
            app.controller("controlador", function($scope, $http) {


                $scope.init = function() {

                    $scope.loadMascotasBarra = function() {

                        $http({
                                method: "post",
                                url: "php/load_mascotas.php",
                                cache: "false",
                                dataType: "json",
                                data: $.param({
                                    'iduser': <?php echo $_SESSION['iduser']; ?>
                                }),
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                }
                            }).success(function(data) {

                                $scope.barramascotas = data;

                            })

                            .error(function(error, status) {
                                $scope.data.error = {
                                    message: error,
                                    status: status
                                };
                                console.log($scope.data.error.status);
                                alert($scope.data.error);

                            });

                    }

                    $scope.loadMascotasInRatio = function() {

                        $http({
                                method: "post",
                                url: "php/load_mascotasbuscpar.php",
                                cache: "false",
                                dataType: "json",
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                }
                            }).success(function(data) {

                                $scope.mascotas = data;

                                $scope.posuser = '<?php include("php/posenmapa.php"); ?>';

                                //SEPARO LAS COORDENADAS EN LAT Y LONG.
                                $scope.posuser = $scope.posuser.split(",");
                                // CREO EL OBJETO DE LA POSICION DEL USUARIO, PERO PRIMERO PARSEO A FLOAT LAT Y LON SI NO NO ANDA
                                var lluser = new google.maps.LatLng(parseFloat($scope.posuser[0]), parseFloat($scope.posuser[1]));
                                var llmasc;
                                var lat;
                                var lon;
                                var cad;
                                var dist;
                                angular.forEach($scope.mascotas, function(value, key) {
                                    
                                    //SEPARO LAS COORDENAS EN LAT Y LONG POR CADA MASCOTA
                                    var cad = value.pos.split(",");
                                    lat = cad[0];
                                    lon = cad[1];
                                    
                                    
                                    // CREO EL OBJETO DE LA POSICION DE LA MASCOTA, PERO PRIMERO PARSEO A FLOAT LAT Y LON SI NO NO ANDA
                                    llmasc = new google.maps.LatLng(parseFloat(lat), parseFloat(lon));
                                    //COMPUTEDISTANCEBETWEEN DEVUELVE LA DIFERENCIA EN METROS, LO PASO A KM DIVIDIENDO POR 1000
                                    dist = (google.maps.geometry.spherical.computeDistanceBetween(lluser, llmasc)) / 1000;
                                    
                                    //INSERT LA DISTANCIA EN CADA MASCOTA.
                                    value.dist = dist;
                                });

                            })

                            .error(function(error, status) {
                                $scope.data.error = {
                                    message: error,
                                    status: status
                                };
                                console.log($scope.data.error.status);
                                alert($scope.data.error);

                            });

                    }

                    $scope.loadRaza = function() {

                        var tipomasc = $("#mimasc").find(':selected').data('idtipo');

                        $http({
                                method: "post",
                                url: "php/load_raza.php",
                                cache: "false",
                                dataType: "json",
                                data: $.param({
                                    'idtipo': tipomasc
                                }),
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                                }
                            }).success(function(data) {

                                $scope.raza2 = data;
                            })

                            .error(function(error, status) {
                                $scope.data.error = {
                                    message: error,
                                    status: status
                                };
                                console.log($scope.data.error.status);
                                alert($scope.data.error);
                                alert(error);
                                alert(status);
                            })

                            .then(function(xhr, textStatus) {
                                //alert(xhr.status);
                                //console.log(xhr.status);
                            });

                    }

                    $scope.loadMascotasBarra();
                    $scope.loadMascotasInRatio();
                }

                $scope.buscarPor = function(prop, val, raza) {


                    return function(item) {

                        var idsexo = $("#mimasc").find(':selected').data('idsexo');
                        var tam = $("#tam").val();

                        if (item[prop] < val && item['idraza'] == raza && item['idtam'] == tam && item['idsexo'] != idsexo) {
                            
                            return true;

                        } else {
                            
                            return false;
                        }
                    }
                }

                $scope.insertaCita = function(idmasc, idpar) {

                    $http({
                            method: "post",
                            url: "php/insertacita.php",
                            cache: "false",
                            dataType: "json",
                            data: ({
                                'idmasc': idmasc,
                                'idpar': idpar
                            }),
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).success(function(data) {

                            alert("Cita arreglada");
                        })

                        .error(function(error, status) {
                            $scope.data.error = {
                                message: error,
                                status: status
                            };
                            console.log($scope.data.error.status);
                            alert($scope.data.error);
                            alert(error);
                            alert(status);
                        })

                        .then(function(xhr, textStatus) {
                            //alert(xhr.status);
                            //console.log(xhr.status);
                        });

                }


            });

        </script>
    </body>

    </html>
