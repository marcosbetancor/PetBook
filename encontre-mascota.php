<?php
session_start();
$_SESSION["perfilmasc"] = $_GET["idmasc"];	
 
?>
<!DOCTYPE HTML>
<html>
<head>
    <link rel="stylesheet" href="bootstrap-3.3.7/dist/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
      <script src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="css/encontre-mascota.css">
       <script src="angular.min.js"></script>
     <link rel="stylesheet" href="dist/css/lightbox.min.css">
    <script src=" dist/js/lightbox-plus-jquery.min.js"></script>
</head>
  
<body ng-app="myapp" ng-controller="controlador" ng-init="init()">
    <div class="logo">
        <img src="img/logo.png" class="logo_img">
    </div>
    
    <div class="titulo">
        me encontraste?
    </div>
    
    <div class="contenedor">
    
        <div class="sub_contenedor" ng-repeat="mascota in mascotas">
            
       
          <a class="example-image-link thumbnail" href="http://localhost/petbookv7.3/img/fer.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward.">
           <img ng-src="{{mascota.imgperf}}" id="foto"class="perfmasc pull-left" />
            
            </a>
            <div>
            <p>
                Nomre:{{mascota.nombre}}<br>
            Fecha de nac.: {{mascota.fecnac}}<br> Sexo: {{mascota.sexo}}<br> Raza: {{mascota.raza}}<br> Tamaño: {{mascota.tam}}<br>
            </p>
                 <?php 
                        require("php/info_usuario.php");
                                                
                         ?>
                <p>
                    datos del dueño:
                    <br>
                    mail:<?php echo $mail;?>
                    <br>
                    telefono:<?php echo $telefono;?>
                </p>
            </div>
        
            
           <button type="button" class="btn btn-success">encontrado</button>
            
        
        
    </div>
    </div>
    
</body>
    <script>
        var app = angular.module("myapp", []);
        app.controller("controlador", function($scope, $http) {

            $scope.init = function() {

                $scope.loadMascotasBarra = function() {

                    $http({
                            method: "post",
                            url: "php/load_encontrar-mascota.php",
                            cache: "false",
                            dataType: "json",
                            data: $.param({
                                'iduser': <?php echo $_SESSION['iduser']; ?>
                            }),
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).success(function(data) {

                            $scope.barramascotas = data;
                            $scope.mascotas = data;

                        })

                        .error(function(error, status) {
                            $scope.data.error = {
                                message: error,
                                status: status
                            };
                            console.log($scope.data.error.status);
                            alert($scope.data.error);

                        });

                }


                $scope.loadMascotasBarra();

            }

           

            
        });

    </script>
</html>
