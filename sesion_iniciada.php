<?php
session_start();
					
if(isset($_SESSION["user"]))
{
    $_SESSION["idmasc"] = $_GET['idmasc'];
}
	else
{
	header("location:index.php");
}
?>
	<!DOCTYPE HTML>
	<html>

	<head>
		<link rel="stylesheet" href="css/iniciomascota.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="bootstrap-3.3.7/dist/css/bootstrap.min.css">
		<script src="bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>

		<link rel="stylesheet" href="css/css_barra.css">
		<link rel="stylesheet" href="css/barra.css">
		<script src="angular.min.js"></script>
	</head>

	<body ng-app="myapp" ng-controller="controlador" ng-init="init()">

		<?php 
            include("header.php"); 
        
            include("barralateral.php");
        ?>

		<!-- ********************************* EMPIEZA CONTAINER ********************************************** -->

		<div class="container text-center">

			<div class="row col-sm-12">

				<!-- ********************************* BARRA IZQUIERDA ********************************************** -->
				<div class="col-sm-3 well">
					<img src="img/<?php echo $img;?>" height="65" width="65" id="imgizq">
					<div class="well">
						<p><a href="#">Interests</a></p>

					</div>
					<div class="alert alert-success fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						<p><strong>Ey!</strong></p>
						parece que aun no ingresaste ninguna mascota, crea su perfil aquí
					</div>



					<div id="myCarousel" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>

						<!--------------------------------------CARRUSEL ADOPTAR-------------------------------------------->
						<div class="carousel-inner">
							<div class="item active" ng-repeat="masc in adoptar">
								<img ng-src="{{masc.imgperf}}"><br>
								<p>Nombre: {{masc.nombre}}</p>
								<p>Raza: {{masc.raza}}</p>
								<p>Sexo: {{masc.sexo}}</p>
								<p>Tamaño: {{masc.tam}}</p>
								<button class="btn btn-primary" id="adoptar" data-idmasc="{{masc.id}}" ng-click="adoptarMascota(masc.id)">
									ADOPTAR
								</button>
							</div>
						</div>

						<!-- Left and right controls -->
						<!--<a class="left carousel-control" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
							<span class="sr-only">Next</span>
						</a>-->
					</div>
				</div>

				<!-- ********************************* CIERRA BARRA IZQUIERDA ********************************************** -->
				<div class="col-sm-7">

					<!-- ********************************* FORMULARIO PUBLICACION ********************************* -->
					<form role="form" method="POST" action="subir_publicacion.php" enctype="multipart/form-data" id="formu">
						<textarea class="form-control" name="text" placeholder="Que estas pensando pichichu?" id="pubtext"></textarea>
						<input type="url" class="form-control hidden" name="urlvideo" id="inputurl" placeholder="www.youtube.com/">
						<div class="row opcpub">
							<label class="btn btn-default btn-file pull-left opcimg opcsub">
                               <i class="glyphicon glyphicon-picture" id="addpicture"></i>
                            	<input type="file" id="addfile" name="file">
                        	</label>
							<label class="btn btn-default pull-left opcsub opcvideo">
                               <i class="glyphicon glyphicon-film "></i>
                        	</label>

							<button type="submit" class="btn btn-primary pull-right">Publicar</button>
						</div>
					</form>

					<!-- ********************************* CIERRA FORMULARIO PUBLICACION ********************************* -->

					<!-- ********************************* CONTENEDOR DE PUBLICACIONES ********************************************** -->

					<div class="contpub" ng-repeat="publicacion in publicaciones">
						<div class="row pub">
							<div class="col-sm-12">
								<div class="panel panel-default text-left">
									<div class="headpanel row-fluid">
										<img src="{{publicacion.imgperf}}" class="img-responsive" />
										<a href="perfil_mascota.php?idmasc={{publicacion.idmasc}}">{{publicacion.nombre}}</a>
										<span class="fecha">{{publicacion.fecha}}</span>
									</div>
									<div class="panel-body">
										<p>
											{{publicacion.cuerpo}}
										</p>
										<div class="contmedia">
											<img src="{{publicacion.url}}" class="img-responsive center-block" ng-if="publicacion.tipourl == 1" />

											<iframe width="560" height="315" class="embed-responsive-item" ng-src="{{publicacion.url}}" frameborder="0" ng-if="publicacion.tipourl == 2" allowfullscreen></iframe>

										</div>
									</div>

									<button type="button" class="btn btn-default btn-sm like blue " data-idpub="{{publicacion.id}}" ng-if="publicacion.mg == 1">
                                                    <span class="glyphicon glyphicon-thumbs-up"></span> Like
                                                </button>

									<button type="button" class="btn btn-default btn-sm like" data-idpub="{{publicacion.id}}" ng-if="publicacion.mg == 0">
                                                    <span class="glyphicon glyphicon-thumbs-up"></span> Like
                                                </button>

									<button type="button" class="btn btn-default btn-sm comment" data-idpub="{{publicacion.id}}" id="botoncomentario">
                                    				<span class="glyphicon glyphicon-comment"></span> Comentar
                               					 </button>
								</div>
								<div class="comentar well col-sm-12 center-block hidden">

									<div class="form-group">
										<div class="input-group">
											<input type="text" class="form-control comentartext " />
											<span class="input-group-btn">
                                                       <button type="button" class="btn btn-default comentarbtn" data-idpub="{{publicacion.id}}">
                                                              <i class="glyphicon glyphicon-send"></i>
                                                       </button>
                                                    </span>
										</div>
									</div>
									<div class="contcomentarios">

									</div>

								</div>
							</div>
						</div>
					</div>
				</div>


				<!-- ********************************* CIERRA CONTENEDOR DE PUBLICACIONES ********************************************** -->


				<!-- ********************************* BARRA DERECHA ********************************************** -->
				<div class="col-sm-2 well">
					<img src="img/crown.png" class="img-responsive" id="coronader" />

					<div class="well">
						General:
					</div>

					<div class="well">
						Likes:
					</div>

					<div class="well">
						Seguidores:
					</div>
				</div>
				<!-- ********************************* CIERRA BARRA DERECHA ********************************************** -->
			</div>
		</div>
		<!-- ********************************* CIERRA CONTAINER ********************************************** -->

		<footer class="container-fluid text-center">

		</footer>
		<script>
			$('#addfile').change(function() {

				$('#addpicture').attr("class", "glyphicon glyphicon-ok");

				$('.opcvideo').addClass("hidden");

			});

		</script>
		<script>
			var app = angular.module("myapp", []);
			app.config(function($sceDelegateProvider) {
				$sceDelegateProvider.resourceUrlWhitelist([
					'self',
					'https://www.youtube.com/**'
				]);
			});
			app.controller("controlador", function($scope, $http) {

				$scope.init = function() {

					$scope.loadMascotasBarra = function() {

						$http({
								method: "post",
								url: "php/load_mascotas.php",
								cache: "false",
								dataType: "json",
								data: $.param({
									'iduser': <?php echo $_SESSION['iduser']; ?>
								}),
								headers: {
									'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
								}
							}).success(function(data) {

								$scope.barramascotas = data;

							})

							.error(function(error, status) {
								$scope.data.error = {
									message: error,
									status: status
								};
								console.log($scope.data.error.status);
								alert($scope.data.error);

							});

					}


					$scope.loadPublicaciones = function() {

						$http({
								method: "post",
								url: "php/load_publicaciones.php",
								cache: "false",
								dataType: "json",
								headers: {
									'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
								}
							}).success(function(data) {

								$scope.publicaciones = data;

							})

							.error(function(error, status) {
								$scope.data.error = {
									message: error,
									status: status
								};
								console.log($scope.data.error.status);
								alert($scope.data.error);

							});

					}

					$scope.loadAdoptarMascota = function() {

						$http({
								method: "post",
								url: "php/load_adoptarMascota.php",
								cache: "false",
								dataType: "json",
								data: ({'iduser': <?php echo $_SESSION['iduser']; ?>}),
								headers: {
									'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
								}
							}).success(function(data) {


								$scope.adoptar = data;
								alert(JSON.stringify($scope.adoptar));

							})

							.error(function(error, status) {
								$scope.data.error = {
									message: error,
									status: status
								};
								console.log($scope.data.error.status);
								alert($scope.data.error);

							});

					}

							  
					$scope.loadMascotasBarra();
					$scope.loadPublicaciones();
					$scope.loadAdoptarMascota();
				}
					
					$scope.adoptarMascota = function(idmasc){
						
						
						$http({
								method: "post",
								url: "php/adoptarmascota.php",
								cache: "false",
								dataType: "json",
								data: ({'idmasc': idmasc, 'iduser': <?php echo $_SESSION['iduser']; ?>}),
								headers: {
									'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
								}
							}).success(function(data) {

								alert(JSON.stringify(data));

							})

							.error(function(error, status) {
								$scope.data.error = {
									message: error,
									status: status
								};
								console.log($scope.data.error.status);
								alert($scope.data.error);

							});
						
					}

			});

		</script>
		<script>
			$(document).on("click", ".like", function(e) {

				e.preventDefault;
				var bot = $(this);
				var idpub = $(this).data("idpub");
				var idmasc = <?php echo $_SESSION["idmasc"]; ?>;

				$.ajax({
					"type": "post",
					"url": "php/likepub.php",
					"data": {
						"idpub": idpub,
						"idmasc": idmasc
					},
					"dataType": "json",
					"cache": "false"
				}).done(function(data) {

					$(bot).toggleClass('blue');

				}).fail(function(error, status) {

					alert(JSON.stringify(error));
					alert(JSON.stringify(status));
				});
			});

		</script>
		<script>
			$(document).on("click", ".comentarbtn", function(e) {

				e.preventDefault;
				var idpub = $(this).data("idpub");
				var idmasc = <?php echo $_SESSION["idmasc"]; ?>;
				window.estecomentar = $(this).parents(".comentar");
				var comtext = $(this).parents(".input-group-btn").siblings(".comentartext").val();

				if (comtext == "" || comtext == " ") {

					alert("Debe ingresar algo que comentar");

				} else {

					$.ajax({
						"type": "post",
						"url": "php/comentar_pr.php",
						"data": {
							"idpub": idpub,
							"idmasc": idmasc,
							"comtext": comtext
						},
						"dataType": "json",
						"cache": "false"
					}).done(function(data) {


						$(window.estecomentar).toggleClass("hidden");


					}).fail(function(error, status) {

						alert(JSON.stringify(error));
						alert(JSON.stringify(status));
					});

				}

			});

		</script>



		<script>
			$(document).on("click", ".comment", function(e) {

				var idpub = $(this).data("idpub");
				window.estecomentar = $(this).parent().siblings(".comentar");
				window.estecomentar.toggleClass("hidden");

				$.ajax({
					"type": "post",
					"url": "php/load_comentarios.php",
					"data": {
						"idpub": idpub
					},
					"dataType": "json",
					"cache": "false"

				}).done(function(comentarios) {

					var todoscom = "";
					var newRow = "";
					var cont = 0;

					$.each(comentarios, function(i, com) {

						newRow =
							"<div class='comentario'>" +
							"<a href=perfil_mascota.php?idmasc=" + com.idmasc + ">" + com.nombre + "</a>" +
							"<p>" + com.cuerpo + "</br>" +
							"<span class='fecha'>" + com.fecha + "</span></p>" +
							"</div>";

						todoscom = todoscom + newRow;
						cont++;


					});

					$(window.estecomentar).find(".contcomentarios").html(todoscom);



				}).fail(function(error, status) {

					alert(JSON.stringify(error));
					alert(JSON.stringify(status));
				});

			});

		</script>
		<script>
			$(document).on("click", ".opcvideo", function(e) {

				if (!$('#inputurl').val().trim()) {

					$('.opcimg').toggleClass("hidden");
				} else {

					$('.opcimg').addClass("hidden");
				}

				$('#inputurl').toggleClass("hidden");

			});

		</script>
		<script>
			$(document).on("click", ".opcvideo", function(e) {



			});

		</script>
	</body>

	</html>
