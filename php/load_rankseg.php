<?php

require("conexion.php");

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);
    $idmasc = $request->idmasc;
    $tiempo = $request->tiempo;

function searchInArray($item , $array){
    return preg_match('/"'.$item.'"/i' , json_encode($array));
}



if($tiempo == 1){
    
    $consulta = "select  masc.id,masc.nombre, count(*) as cant from seguidores as seg
                join mascota as masc on seg.idmasc = masc.id
                 join raza on raza.id = masc.idraza
                where DATE(seg.fecha) = DATE(NOW())
                group by seg.idmasc, raza.idtipo
                HAVING raza.idtipo = (select idtipo from mascota
                                     join raza on raza.id = mascota.idraza
                                     where mascota.id = $idmasc)
                order by cant desc
                limit 5;";
    
} else if($tiempo == 2){
    
    $consulta = "select  masc.id,masc.nombre, count(*) as cant from seguidores as seg
                join mascota as masc on seg.idmasc = masc.id
                 join raza on raza.id = masc.idraza
                where DATE(seg.fecha) >= date_sub(NOW(),interval 7 day) or DATE(seg.fecha) <= DATE(NOW())
                group by seg.idmasc, raza.idtipo
                HAVING raza.idtipo = (select idtipo from mascota
                                     join raza on raza.id = mascota.idraza
                                     where mascota.id = $idmasc)
                order by cant desc
                limit 5;";
    
} else {
    
    $consulta = "select  masc.id,masc.nombre, count(*) as cant from seguidores as seg
                join mascota as masc on seg.idmasc = masc.id
                join raza on raza.id = masc.idraza
                group by seg.idmasc, raza.idtipo
                HAVING raza.idtipo = (select idtipo from mascota
                                         join raza on raza.id = mascota.idraza
                                         where mascota.id = $idmasc)
                order by cant desc
                limit 5;";
    
}

$output = array();  

$result = mysqli_query($conexion,$consulta);

 while($row = mysqli_fetch_array($result))   
     {  	
            array_push($output,$row);
	 		
     }  

if(!searchInArray($idmasc,$output)){
 
    if($tiempo == 1){
    
        $consulta = "select  masc.id,masc.nombre, count(*) as cant from seguidores as seg
                join mascota as masc on seg.idmasc = masc.id
                 join raza on raza.id = masc.idraza
                where DATE(seg.fecha) = DATE(NOW()) and masc.id = $idmasc
                group by seg.idmasc, raza.idtipo
                HAVING raza.idtipo = (select idtipo from mascota
                                     join raza on raza.id = mascota.idraza
                                     where mascota.id = $idmasc)
                order by cant desc
                limit 5;";
    
    
    } else if($tiempo == 2){
    
         $consulta = "select  masc.id,masc.nombre, count(*) as cant from seguidores as seg
                join mascota as masc on seg.idmasc = masc.id
                 join raza on raza.id = masc.idraza
                where DATE(seg.fecha) >= date_sub(NOW(),interval 7 day) or DATE(seg.fecha) <= DATE(NOW()) and masc.id = $idmasc
                group by seg.idmasc, raza.idtipo
                HAVING raza.idtipo = (select idtipo from mascota
                                     join raza on raza.id = mascota.idraza
                                     where mascota.id = $idmasc)
                order by cant desc
                limit 5;";
    
        } else {
    
          $consulta = "select  masc.id,masc.nombre, count(*) as cant from seguidores as seg
                join mascota as masc on seg.idmasc = masc.id
                join raza on raza.id = masc.idraza
                group by seg.idmasc, raza.idtipo
                where masc.id = $idmasc
                HAVING raza.idtipo = (select idtipo from mascota
                                         join raza on raza.id = mascota.idraza
                                         where mascota.id = $idmasc)
                order by cant desc
                limit 5;";
    }
    
    $respos = mysqli_query($conexion,$consulta);

    while($row = mysqli_fetch_array($respos))   
     {  	
            array_push($output,$row);
	 		
     } 
    
}

    function utf8ize($d) {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = utf8ize($v);
            }
        } else if (is_string ($d)) {
            return utf8_encode($d);
        }
        return $d;
    }

     echo json_encode(utf8ize($output)); 

?>
