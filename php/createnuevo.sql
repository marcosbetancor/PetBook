DROP DATABASE IF EXISTS petbook;

CREATE DATABASE petbook;

USE petbook;

 CREATE TABLE sexousuario(
     id INT AUTO_INCREMENT PRIMARY KEY,
     descrip VARCHAR(50));

CREATE TABLE usuario(
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100),
    apellido VARCHAR(100),
    idsexo INT,
    posicion VARCHAR(50),
    mail VARCHAR(100) UNIQUE,
    pass VARCHAR(100),
    fecnac DATE,
    CONSTRAINT Usuario_IdSexo_FK FOREIGN KEY (idsexo) 
    REFERENCES sexousuario(id));
     
 CREATE TABLE tel(
     idusuario INT,
     tel VARCHAR(20),
     PRIMARY KEY(idusuario, tel),
     CONSTRAINT Tel_IdUsuario_FK FOREIGN KEY (idusuario)
     REFERENCES usuario(id));     

CREATE TABLE fotousuario(
    id INT AUTO_INCREMENT PRIMARY KEY,
    idusuario INT,
    fechasub DATETIME,
    url VARCHAR(200),
    enuso BOOLEAN,
    CONSTRAINT FotoUsuario_IdUsuario_FK FOREIGN KEY (idusuario)
    REFERENCES usuario(id));
    
 /* CIERRA USUARIO */   
 /* ABRE MASCOTA */
 
CREATE TABLE tipofoto(
    id INT AUTO_INCREMENT PRIMARY KEY,
    descrip VARCHAR(50));
    
   CREATE TABLE tipomasc(
        id INT AUTO_INCREMENT PRIMARY KEY,
        descrip VARCHAR(50));
    
    CREATE TABLE raza(
        id INT AUTO_INCREMENT PRIMARY KEY,
        descrip VARCHAR(100),
        idtipo INT,
        CONSTRAINT Raza_IdTipo_FK FOREIGN KEY (idtipo)
   	    REFERENCES tipomasc(id));
        
    CREATE TABLE sexomasc(
        id INT AUTO_INCREMENT PRIMARY KEY,
        descrip VARCHAR(50));
        
     CREATE TABLE tammasc(
         id INT AUTO_INCREMENT PRIMARY KEY,
         descrip VARCHAR(50));    
    
  CREATE TABLE mascota(
        id INT AUTO_INCREMENT PRIMARY KEY,
        nombre VARCHAR(100),
        idusuario INT,
        fecnac DATE,
        idraza INT,
        idsexo INT,
        idtam INT,
        buscpar BOOLEAN,
        enadop BOOLEAN,
        perdido BOOLEAN,
    CONSTRAINT Mascota_IdUsuario_FK FOREIGN KEY (idusuario)
    REFERENCES usuario(id),
    CONSTRAINT Mascota_IdRaza_FK FOREIGN KEY (idraza)
    REFERENCES raza(id),
    CONSTRAINT Mascota_IdSexo_FK FOREIGN KEY (idsexo)
    REFERENCES sexomasc(id),
    CONSTRAINT Mascota_IdTam_FK FOREIGN KEY (idtam)
    REFERENCES tammasc(id));    
    
CREATE TABLE fotomasc(
    id INT AUTO_INCREMENT PRIMARY KEY,
    idmasc INT,
    idusuario INT,
    fechasub DATETIME,
    url VARCHAR(200),
    enuso BOOLEAN,
    CONSTRAINT FotoMasc_IdMasc_FK FOREIGN KEY (idmasc)
    REFERENCES mascota(id),
    CONSTRAINT FotoMasc_IdUsuario_FK FOREIGN KEY (idusuario)
    REFERENCES usuario(id));

CREATE TABLE publicacion (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    idusuario INT,
    idmasc INT,
    cuerpo VARCHAR(144),
    fecha datetime,
    CONSTRAINT Publicacion_IdUsuario_FK FOREIGN KEY (idusuario)
        REFERENCES usuario (id),
    CONSTRAINT Publicacion_IdMasc_FK FOREIGN KEY (idmasc)
        REFERENCES mascota (id)
);


CREATE TABLE pub_com (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    idpub BIGINT,
    idmasc int,
    cuerpo VARCHAR(144),
    fecha datetime,
    CONSTRAINT PubCom_IdPub_FK FOREIGN KEY (idpub)
        REFERENCES publicacion (id),
    CONSTRAINT PubCom_IdMasc_FK FOREIGN KEY (idmasc)
        REFERENCES mascota (id)
);
        
CREATE TABLE pub_imag_tipo (
    id INT AUTO_INCREMENT PRIMARY KEY,
    descrip VARCHAR(50)
);
        
CREATE TABLE pub_imag (
    idpub BIGINT PRIMARY KEY,
    url VARCHAR(200),
    idtipo INT,
    CONSTRAINT PubImag_IdPub_FK FOREIGN KEY (idpub)
        REFERENCES publicacion (id)
);
           
CREATE TABLE pub_mg (
    idpub BIGINT,
    idmasc INT,
    fecha datetime,
    PRIMARY KEY (idpub , idmasc),
    CONSTRAINT PubMg_IdPub_FK FOREIGN KEY (idpub)
        REFERENCES publicacion (id),
    CONSTRAINT PubMg_IdMasc_FK FOREIGN KEY (idmasc)
        REFERENCES mascota (id)
);
                
CREATE TABLE seguidores (
    idmasc INT,
    idseg INT,
    fecha datetime,
    PRIMARY KEY (idmasc , idseg),
    CONSTRAINT Seguidores_IdMasc_FK FOREIGN KEY (idmasc)
        REFERENCES mascota (id),
    CONSTRAINT Seguidores_IdSeg_FK FOREIGN KEY (idseg)
        REFERENCES mascota (id)
);
                
CREATE TABLE pareja (
    idmasc INT,
    idpar INT,
    fecha datetime,
    PRIMARY KEY (idmasc , idpar),
    CONSTRAINT Pareja_IdMasc_FK FOREIGN KEY (idmasc)
        REFERENCES mascota (id),
    CONSTRAINT Pareja_IdPar_FK FOREIGN KEY (idpar)
        REFERENCES mascota (id)
);
               

delimiter $$

CREATE PROCEDURE pubmg(idpubp BIGINT, idmascp INT)
BEGIN

IF EXISTS (SELECT * FROM pub_mg WHERE idpub= idpubp AND idmasc = idmascp) THEN
    DELETE FROM pub_mg WHERE idpub= idpubp AND idmasc = idmascp;
ELSE 
    INSERT INTO pub_mg VALUES(idpubp,idmascp,now());
END IF;

END $$

delimiter ||

CREATE PROCEDURE seguirmasc(idmascp INT, idsegp INT)
BEGIN

IF EXISTS (SELECT * FROM seguidores WHERE idmasc= idmascp AND idmasc = idsegp) THEN
    DELETE FROM seguidores WHERE idmasc= idmascp AND idmasc = idsegp;
ELSE 
    INSERT INTO seguidores VALUES(idmascp,idsegp,now());
END IF;

END ||


delimiter $$

CREATE PROCEDURE cambiabuscpar(idmascp INT)
BEGIN

IF (select buscpar from mascota where id = idmascp) 

	THEN
    
		UPDATE mascota SET buscpar = false where id = idmascp;
ELSE 

		UPDATE mascota SET buscpar = true where id = idmascp;
END IF;

END $$


delimiter $$

CREATE PROCEDURE cambiaperdido(idmascp INT)
BEGIN

IF (select perdido from mascota where id = idmascp) 

	THEN
    
		UPDATE mascota SET perdido = false where id = idmascp;
ELSE 

		UPDATE mascota SET perdido = true where id = idmascp;
END IF;

END $$

CREATE PROCEDURE cambiaadop(idmascp INT)
BEGIN

IF (select enadop from mascota where id = idmascp) 

	THEN
		UPDATE mascota SET enadop = false where id = idmascp;
ELSE 
		UPDATE mascota SET enadop = true where id = idmascp;
END IF;

END $$



select mascota.fecnac,mascota.nombre, tammasc.descrip as desTam,sexomasc.descrip as desSex,raza.descrip as desRaza from mascota inner join tammasc on mascota.id=tammasc.id 
inner join sexomasc on mascota.idsexo=sexomasc.id inner join raza on mascota.idraza=raza.id
where mascota.id='1';

select * from usuario;