<?php  
    require("conexion.php");

     $output = array();  

     $query = "SELECT mascota.id as id, mascota.nombre, mascota.fecnac,  raza.id as idraza, raza.descrip as raza, sexomasc.id as idsexo, sexomasc.descrip as sexo ,tammasc.id as idtam, tammasc.descrip as tam , buscpar, enadop, perdido, substring(fotomasc.url, instr(fotomasc.url, '/')+1) as imgperf, usuario.posicion as pos FROM mascota
               JOIN raza on raza.id = mascota.idraza
               JOIN sexomasc on sexomasc.id = mascota.idsexo
               JOIN tammasc on tammasc.id = mascota.idtam
               JOIN fotomasc on fotomasc.idmasc = mascota.id
               JOIN usuario on usuario.id = mascota.idusuario
               WHERE mascota.buscpar = true and fotomasc.enuso = true;";  

 $result = mysqli_query($conexion, $query); 

 while($row = mysqli_fetch_array($result))   
     {  
            array_push($output,$row);
     }  

    function utf8ize($d) {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = utf8ize($v);
            }
        } else if (is_string ($d)) {
            return utf8_encode($d);
        }
        return $d;
    }

     echo json_encode(utf8ize($output));  
 ?>
