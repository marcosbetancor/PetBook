<?php  
    require("conexion.php");

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);
    $idmasc = $request->idmasc;
    $iduser = $request->iduser;
    
     $query = "SELECT mascota.id as id, nombre, fecnac, raza.descrip as raza, sexomasc.descrip as sexo , tammasc.descrip as tam , buscpar, enadop, perdido,  substring(fotomasc.url, instr(fotomasc.url, '/')+1) as imgperf FROM mascota
               JOIN raza on raza.id = mascota.idraza
               JOIN sexomasc on sexomasc.id = mascota.idsexo
            	JOIN fotomasc on fotomasc.idmasc = mascota.id
               JOIN tammasc on tammasc.id = mascota.idtam;";  

    $queryfotos = "SELECT id, substring(url, instr(url, '/')+1) as url from fotomasc
                   WHERE idmasc = $idmasc and idusuario = $iduser;";


//CONSIGO LA MASCOTA
 $result = mysqli_query($conexion, $query); 

// LO PASO A UN ARRAY
  $row = mysqli_fetch_array($result);

//CONSIGO LAS FOTOS
  $resfotos = mysqli_query($conexion, $queryfotos);

    
//LAS PASO A UN ARRAY
  $fotos = mysqli_fetch_array($resfotos);
    
    $row["fotos"] = $fotos;

    function utf8ize($d) {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = utf8ize($v);
            }
        } else if (is_string ($d)) {
            return utf8_encode($d);
        }
        return $d;
    }

     echo json_encode(utf8ize($row));  
 ?>
