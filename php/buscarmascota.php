<?php  
    require("conexion.php");

    
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);
    $buscar = $request->buscar;
    $idmasc = $request->idmasc;

     $output = array();  

     $query = "SELECT mascota.id as id, nombre, fecnac, raza.descrip as raza, sexomasc.descrip as sexo , tammasc.descrip as tam , buscpar, enadop, perdido, fotomasc.url as imgperf, IF( mascota.id in(select idmasc from seguidores
												where idseg =" . $idmasc . "), true, false) as seg FROM mascota
               JOIN raza on raza.id = mascota.idraza
               JOIN sexomasc on sexomasc.id = mascota.idsexo
               JOIN tammasc on tammasc.id = mascota.idtam
               JOIN fotomasc on fotomasc.idmasc = mascota.id
               where mascota.nombre like '%$buscar%';";

 $result = mysqli_query($conexion, $query); 

 while($row = mysqli_fetch_array($result))   
     {  
            array_push($output,$row);
     }  

    function utf8ize($d) {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = utf8ize($v);
            }
        } else if (is_string ($d)) {
            return utf8_encode($d);
        }
        return $d;
    }

     echo json_encode(utf8ize($output));  
 ?>
