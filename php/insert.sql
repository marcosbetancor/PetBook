insert into sexousuario values
	(null, 'Masculino'),
    (null, 'Femenino'),
    (null, 'Otro');

insert into tipofoto values
	(null, 'Perfil'),
    (null, 'Mascota');

insert into sexomasc values
	(null,'Macho'),
    (null, 'Hembra');
    
insert into tammasc values
	(null, 'chico'),
    (null, 'Mediano'),
    (null, 'Grande');
    
insert into tipomasc values
	(null, "Perro"),
	(null, "Gato"),
	(null, "Conejo");

/* RAZA DE PERROS */
insert into raza values
	(null, "Dogo", 1),
    (null, "Golden Retriever", 1),
    (null, "Labrador", 1),
    (null, "Cocker", 1),
    (null, "Chiuaua", 1),
    (null, "Boyero de Berna", 1),
    (null, "Fox Terrier", 1),
    (null, "Otro", 1);
    
/* RAZA DE GATOS */
insert into raza values
	(null, "Siames", 2),
	(null, "Persa", 2),
	(null, "Maine Coon", 2),
	(null, "Burmés", 2),
	(null, "Bengala", 2),
	(null, "Bobtail Americano", 2),
	(null, "Siberiano", 2),
    (null, "Otro", 2);
    
/* RAZA DE CONEJOS */
insert into raza values
	(null, "Mini Lop", 3),
	(null, "Azul de Viena", 3),
	(null, "Holandés", 3),
	(null, "Californiano", 3),
	(null, "Silver Fox", 3),
	(null, "Otro", 3);

/* TIPOS DE IMAGENES EN PUBLICACIONES(IMAGEN O VIDEO)*/
insert into pub_imag_tipo values
	(null, "Imagen"),
	(null, "Video");
