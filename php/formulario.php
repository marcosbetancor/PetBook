<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    
    <!-- Empieza script GOOGLE MAPS API -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuylZC7LDwp8B7q8wB4lXQPGHvD1f7TZQ"></script>
    <script>
        function loadMap() {
            
            var cont = 0;
            
            var mapOptions = {
                
                center:new google.maps.LatLng(-34.6686986,-58.5614947),
                zoom:12,
                mapTypeId:google.maps.MapTypeId.ROADMAP  
            };
            
            var map = new google.maps.Map(document.getElementById("mapa"),mapOptions);
            
            var marker;

            function placeMarker(location) {
              if ( marker ) {
                marker.setPosition(location);
              } else {
                marker = new google.maps.Marker({
                  position: location,
                  map: map
                });
              }
            }

            google.maps.event.addListener(map, 'click', function(event) {
        
                placeMarker(event.latLng);
                
                $('#pos').text(event.latLng);
                
                $('#pos').val(event.latLng);
                
            });
            
        }
    </script>
    <!-- CIERRA SCRIPT GOOGLE MAPS API -->

    
    
    
<link href="../css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-ui.min.js"></script>
<script>
    $(function() {
        $("#datepicker").datepicker();
    });
</script>
     
    
    
     <script src="../js/formulario.js"></script>
    	<script type="text/javascript" src="../jquery-ui-1.11.4/jquery-ui.js"></script>
    
   <link rel="stylesheet" type="text/css" href="../css/form.css">
</head>

<body>
    
<div id="contenedor">
<div class="container">  
                <form role="form" id="formu" method="POST" action="registrar_pr.php" enctype="multipart/form-data">

                    <div class="form-group">
                        <label class="sr-only" for="ejemplo_email_2">nombre</label>
                        <input type="text" class="form-control" 
                               placeholder="Nombre" maxlength= 50 name="nombre" id="nombre_input" />
                    </div>

                    <div class="form-group">
                        <label class="sr-only" for="ejemplo_email_2">apellido</label>
                        <input type="text" class="form-control"
                               placeholder="Apellido" maxlength=50 name="apellido" 
                               id="apellido_input"/>
                    </div>

                    <div class="form-group">

                        Hombre:<input type="radio" value="1" onclick="marcado=true"class="sexo_input" name="sexo" />
                        Mujer:<input type="radio" value="2" onclick="marcado=true"class="sexo_input" name="sexo" />
                        Otro:<input type="radio" value="3" onclick="marcado=true"class="sexo_input" name="sexo" />

                    </div>
                    
                    <div class="form-group">
                        <label for="fechanac">Fecha de nacimiento:</label>
                    <input name="fecnac" maxlength=10 class="  date-picker form-control" id="datepicker" placeholder="fecha de nac."/>
                    </div>
                    <div class="form-group">
                        <label class="sr-only">Email</label>
                        <input class="form-control" type="email" placeholder="Email" name="email" maxlength=100 id="mail_input"/>
                    </div>

                    <div class="form-group">
                        <label class="sr-only">Password</label>
                        <input class="form-control" type="password" placeholder="Contraseña" name="contra" maxlength=100 id="contra_input"/>
                    </div>
                   
                    <div class="form-group">
                        <label class="sr-only">Tel&eacute;fono</label>
                        <input class="form-control" type="text" maxlength=10 placeholder="Teléfono" name="telefono" id="tel" />
                    </div>

                    <div class="form-group">
                        <label class="sr-only">Posici&oacute;</label>
                        <input class="form-control" type="text" placeholder="Posición" name="posicion" id="pos" />
                    </div>
					
					 <div class="form-group">
                        <label class="sr-only">Foto</label>
                        <tr bgcolor="skyblue">
						<td bgcolor="skyblue"><strong>Foto:</strong></td>  <td><input type="file" name="foto" id="foto"></td>
						</tr>
                     </div>
					
                    
           
            <button type="button"class="btn btn-success" name="enviar" onclick="validacion_formulario()"  height="50px"width="310px">Registrarse</button>
                    
    </form>
    </div>
				<div class="sub_contenedor_mapa">
                    Seleccioná tu ubicacion
                    <div id="mapa"></div>
				</div>
				</div>
				
<script>
            $(document).ready(function () {
                $body = $("body");

                $(document).on({
                    ajaxStart: function() { $body.addClass("loading");    },
                    ajaxStop: function() { $body.removeClass("loading"); }    
                });
            });
        </script>
        <script>
             $.datepicker.regional['es'] = {
             closeText: 'Cerrar',
             prevText: '<Ant',
             nextText: 'Sig>',
             currentText: 'Hoy',
             monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
             monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
             weekHeader: 'Sm',
             dateFormat: 'dd-mm-yy',
             firstDay: 1,
             isRTL: false,
             showMonthAfterYear: false,
             maxDate: "0",
             showButtonPanel: true,
             yearSuffix: ''
             };
             $.datepicker.setDefaults($.datepicker.regional['es']);
        </script>
        
				
<script> //INICIA MAPA
    $('document').ready(function(){
       
       loadMap();
        
    });

</script>
</body>
</html>
