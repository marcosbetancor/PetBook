<?php  
    require("conexion.php");

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);
    $idmasc = $request->idmasc;

    $query = "CALL cambiabuscpar($idmasc);";

	if($conexion->query($query)){

		echo json_encode("ok");

	} else {

		try {   
			throw new Exception("MySQL error $conexion->error <br> Query:<br> $query", $conexion->errno);   
		} catch(Exception $e ) {
			echo "Error No: ".$e->getCode(). " - ". $e->getMessage() . "<br >";
			echo nl2br($e->getTraceAsString());
		}
}

