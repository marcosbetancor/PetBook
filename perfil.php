<?php
session_start();
					
if(isset($_SESSION["user"]))
{
						
}
	else
{
	header("location:index.php");
}
?>
<!DOCTYPE HTML>
<html>

<head>
    <scrip>
        
    </scrip>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
     <link rel="stylesheet" href="bootstrap-3.3.7/dist/css/bootstrap.min.css">
    <script src="js/holder/holder.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
   <script src="bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/boton.js"></script>
    <link rel="stylesheet" href="css/perfil.css">
    <link rel="stylesheet" href="css/css_barra.css">
    <link rel="stylesheet" type="text/css" href="css/barra.css">
    <script src="angular.min.js"></script>
    <link rel="stylesheet" href="dist/css/lightbox.min.css">
    <script src=" dist/js/lightbox-plus-jquery.min.js"></script>
    <!-- Empieza script GOOGLE MAPS API -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuylZC7LDwp8B7q8wB4lXQPGHvD1f7TZQ"></script>
    <script>
        function loadMap() {

            var cont = 0;

            var mapOptions = {

                center: new google.maps.LatLng(-34.6686986, -58.5614947),
                zoom: 12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("mapa"), mapOptions);

            var marker = new google.maps.Marker({

                position: new google.maps.LatLng(<?php include("php/posenmapa.php"); ?>),
                map: map

            });



        }


    </script>
    <!-- CIERRA SCRIPT GOOGLE MAPS API -->
</head>

<body ng-app="myapp" ng-controller="controlador" ng-init="init()">
    <?php 
        include("header.php"); 
        
        include("barralateral.php");
    ?>


    <!---cuerpo de pagina-->


    <div class="container col-lg-12 col-sm-6">
        <div class="card hovercard">
            <div class="card-background">
                <img class="card-bkimg" alt="" src="img/fondoPerfil.jpg">
                <!-- http://lorempixel.com/850/280/people/9/ -->
            </div>
            <div class="useravatar">
                <img alt="" src="img/<?php echo $img;?>">



            </div>
            <div class="card-info"> <span class="card-title">
					<?php
					
						echo $_SESSION["user"];
					
					?></span>

            </div>
        </div>
        <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
            <div class="btn-group" role="group">
                <button type="button" id="stars" class="btn btn-success" href="#tab1" data-toggle="tab"><i class="fa fa-paw" aria-hidden="true"></i>
                <div class="hidden-xs">Mascotas</div>
            </button>
            </div>
            <div class="btn-group" role="group">
                <button type="button" id="favorites" class="btn btn-default" href="#tab2" data-toggle="tab"><i class="fa fa-camera" aria-hidden="true"></i>
                <div class="hidden-xs">Fotos</div>
            </button>
            </div>
            <div class="btn-group" role="group">
                <button type="button" id="following" class="btn btn-default" href="#tab3" data-toggle="tab"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                <div class="hidden-xs">informacion</div>
            </button>
            </div>
        </div>

        <div class="well">
            <div class="tab-content">

                <!-- ************************************* LISTADO MASCOTAS ************************************* -->



                <div class="tab-pane fade in active" id="tab1">


                    <div class="panel panel-success" ng-repeat="mascota in mascotas">
                        <div class="panel-heading">{{mascota.nombre}}</div>
                        <div class="panel-body">
                            <img ng-src="{{mascota.imgperf}}" class="perfmasc pull-left" />
                            <div class="infomascota ">
                                <p>
                                   Fecha de nac.: {{mascota.fecnac}}<br> Sexo: {{mascota.sexo}}<br> Raza: {{mascota.raza}}<br> Tamaño: {{mascota.tam}}<br>
                                </p>
                                <a href="perfil_mascota.php?idmasc={{mascota.id}}" class="btn btn-primary">Perfil -></a>
                                <a href="" class="btn btn-danger buscpar" ng-click="cambiabuscpar($event)" ng-if="mascota.buscpar == true" data-idmasc="{{mascota.id}}">No buscar par. -</a>
                                <a href="" class="btn btn-primary" ng-click="cambiabuscpar($event)" ng-if="mascota.buscpar == false" data-idmasc="{{mascota.id}}">Busca pareja</a>
                                <a href="" class="btn btn-danger" ng-click="cambiaperdido($event)" ng-if="mascota.perdido == true" data-idmasc="{{mascota.id}}">Cambia a Encontrado</a>
                                <a href="" class="btn btn-primary" ng-click="cambiaperdido($event)" ng-if="mascota.perdido == false" data-idmasc="{{mascota.id}}">Cambiar a Perdido</a>
                                <a href="" class="btn btn-danger" ng-click="cambiaadop($event)" ng-if="mascota.enadop == true" data-idmasc="{{mascota.id}}">No dar en adopcion</a>
                                <a href="" class="btn btn-primary" ng-click="cambiaadop($event)" ng-if="mascota.enadop == false" data-idmasc="{{mascota.id}}">Dar en adopcion</a>

                            </div>
                        </div>
                    </div>

                    <div class="panel panel-success">
                        <div class="panel-heading">nueva mascota</div>

                        <div class="panel-body">
                            <a href="crear-perfil-mascotas.php"><img src="img/nueva_masc.png" class="img-circle pull-left" height=50px;></a>
                        </div>
                    </div>
                </div>



                <!-- ************************************* CIERRA LISTADO MASCOTAS ************************************* -->



                <div class="tab-pane fade in" id="tab2">
                    <div class="panel panel-success">
                        <div class="panel-heading">Galeria</div>

                        <div class="row">
                            <div class="col-sm-6 col-md-3">

                                <!----si se cambia de version, cambiar el numero de vercion en el link de abajo  --->

                                <a class="example-image-link thumbnail" href="http://localhost/petbookv7.3/img/<?php echo $img;?>" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img height="auto" width="250" src="img/<?php echo $img;?>" alt=""/></a>


                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img data-src="holder.js/250x250" alt="...">
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img data-src="holder.js/250x250" alt="...">
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img data-src="holder.js/250x250" alt="...">
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img data-src="holder.js/250x250" alt="...">
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img data-src="holder.js/250x250" alt="...">
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img src="holder.js/250x250/social">
                                </a>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <a href="#" class="thumbnail">
                                    <img data-src="holder.js/250x250/" alt="...">
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade in" id="tab3">
                    <!---magia here-->
                    <div class="panel panel-success">
                        <div class="panel-heading">editar info</div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-2">

                                </div>
                                <div class="col-md-8" id="info_usuario">
                                    <?php 
                        require("php/info_usuario.php");
                                                
                         ?>
                                    <div>
                         	<span class="glyphicon glyphicon-user" aria-hidden="true"></span> Nombre:
										<?php echo $_SESSION["user"];?>
									</div>
									<div>
									<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Email:
										<?php echo $mail;?> </div>
									<div>
									<span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Fecha de nacimiento:
										<?php echo $fecnac;?> </div>
									<div> 
									<span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span> Telefono:
										<?php echo $telefono;?>
									</div>
                                    <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>Ubicaci&oacute;n:
                                    <div class="iframe-container">
                                        <div id="mapa"></div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var app = angular.module("myapp", []);
        app.controller("controlador", function($scope, $http) {


            $scope.init = function() {

                $scope.loadMascotasBarra = function() {

                    $http({
                            method: "post",
                            url: "php/load_mascotas.php",
                            cache: "false",
                            dataType: "json",
                            data: $.param({
                                'iduser': <?php echo $_SESSION['iduser']; ?>
                            }),
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                            }
                        }).success(function(data) {

                            $scope.barramascotas = data;
                            $scope.mascotas = data;

                        })

                        .error(function(error, status) {
                            $scope.data.error = {
                                message: error,
                                status: status
                            };
                            console.log($scope.data.error.status);
                            alert($scope.data.error);

                        });

                }


                $scope.loadMascotasBarra();

            }

            $scope.cambiabuscpar = function(e) {

                $scope.elem = e.target;
                $scope.idmasc = $(e.target).data("idmasc");

                $http({
                        method: "post",
                        url: "php/cambiabuscpar.php",
                        cache: "false",
                        dataType: "json",
                        data: {
                            'idmasc': $scope.idmasc
                        },
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    }).success(function(data) {

                        if ($($scope.elem).hasClass("btn-danger")) {

                            $($scope.elem).removeClass("btn-danger");
                            $($scope.elem).addClass("btn-primary");
                            $($scope.elem).text("Busca pareja")


                        } else {

                            $($scope.elem).removeClass("btn-primary");
                            $($scope.elem).addClass("btn-danger");
                            $($scope.elem).text("No buscar pareja");
                        }


                    })

                    .error(function(error, status) {
                        $scope.data.error = {
                            message: error,
                            status: status
                        };
                        console.log($scope.data.error.status);
                        alert($scope.data.error);

                    });

            }

            $scope.cambiaadop = function(e) {

                $scope.elem = e.target;
                $scope.idmasc = $(e.target).data("idmasc");

                $http({
                        method: "post",
                        url: "php/cambiaadop.php",
                        cache: "false",
                        dataType: "json",
                        data: {
                            'idmasc': $scope.idmasc
                        },
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    }).success(function(data) {

                        if ($($scope.elem).hasClass("btn-danger")) {

                            $($scope.elem).removeClass("btn-danger");
                            $($scope.elem).addClass("btn-primary");
                            $($scope.elem).text("Dar en adopcion")


                        } else {

                            $($scope.elem).removeClass("btn-primary");
                            $($scope.elem).addClass("btn-danger");
                            $($scope.elem).text("No dar en adopcion");
                        }


                    })

                    .error(function(error, status) {
                        $scope.data.error = {
                            message: error,
                            status: status
                        };
                        console.log($scope.data.error.status);
                        alert($scope.data.error);

                    });

            }

            $scope.cambiaperdido = function(e) {

                $scope.elem = e.target;
                $scope.idmasc = $(e.target).data("idmasc");

                $http({
                        method: "post",
                        url: "php/cambiaperdido.php",
                        cache: "false",
                        dataType: "json",
                        data: {
                            'idmasc': $scope.idmasc
                        },
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    }).success(function(data) {


                        if ($($scope.elem).hasClass("btn-danger")) {

                            $($scope.elem).removeClass("btn-danger");
                            $($scope.elem).addClass("btn-primary");
                            $($scope.elem).text("Cambia a Perdido")


                        } else {

                            $($scope.elem).removeClass("btn-primary");
                            $($scope.elem).addClass("btn-danger");
                            $($scope.elem).text("Cambia a Encontrado");
                        }


                    })

                    .error(function(error, status) {
                        $scope.data.error = {
                            message: error,
                            status: status
                        };
                        console.log($scope.data.error.status);
                        alert($scope.data.error);

                    });

            }
        });

    </script>
    <script>
        //INICIA MAPA
        $('document').ready(function() {

            loadMap();

        });

    </script>

</body>

</html>
