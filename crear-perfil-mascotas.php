<?php
session_start();
					
if(isset($_SESSION["user"]))
{
						
}
	else
{
	header("location:index.php");
}
?>
<!DOCTYPE html>
<html>

<head>
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="bootstrap-3.3.7/dist/css/bootstrap.min.css">
		<script src="bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/barra.css">
    <link rel="stylesheet" href="css/css_barra.css">
    <link rel="stylesheet" href="css/css-crear-perfil-mascotas.css">
    <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <script src="js/jquery-ui.min.js"></script>
    <script>
        $(function() {
            $("#datepicker").datepicker();
        });

    </script>
    <script src="angular.min.js"></script>
</head>

<body ng-app="myapp" ng-controller="controlador" ng-init="init()">
    <?php 
        include("header.php"); 
        
        include("barralateral.php");
        ?>
   <div class="row">

        <div class="col-md-12 col-xs-12">
            <div class="crear_perfil_mascota">
                Crear perfil de mascota
                <div>
                </div>
            </div>


            <div class="contenedor2">

                <div class="sub_contenedor">
                    <div class="centrar_formu">

                        <form role="form" id="formu" method="POST" action="php/registrarmascota_pr.php" enctype="multipart/form-data">

                            <div class="form-group">
                                <label class="sr-only">nombre</label>
                                <input type="text" class="form-control" placeholder="Nombre" name="nombre" />
                            </div>
                                   <input type="hidden" name="idusuario" value='<?php echo $_SESSION['iduser']; ?>'/> 
                                   
                                    <div class="form-group">
                                        <select name="tipomasc" ng-model="tipomasc" ng-change="loadRaza()">  
                                            <option value="">Seleccione tipo</option>  
                                            <option ng-repeat="tipomasc in tipomasc2" value="{{tipomasc.id}}">{{tipomasc.descrip}}</option>  
                                         </select>
                                    </div>

                                    <div class="form-group">
                                        <select name="raza" ng-model="raza">  
                                              <option value="">Seleccione raza</option>  
                                              <option ng-repeat="raza in raza2" value="{{raza.id}}">  
                                                   {{raza.descrip}}  
                                              </option>  
                                         </select>
                                    </div>
                            <div class="form-group">
                                <label class="sr-only"></label>
                                <select class="form-control" title="tamaño" name="tam" style="height: 60%">
                                    <option value="" >Tama&ntilde;o</option>
                                    <option value="1">Chico</option>
                                    <option value="2">Mediano</option>
                                    <option value="3">Grande</option>
                                </select>
                            </div>

                            <div class="form-group">
                                Macho:<input type="radio" value="1" name="sexo" /> 
                                Hembra:<input type="radio" value="2" name="sexo" />

                            </div>
                            <div class="form-group">
                                <label for="fechanac">Fecha de nacimiento:</label>
                                <input name="fecnac" maxlength=10 class="  date-picker form-control" id="datepicker" placeholder="fecha de nac." />
                            </div>
                            <div class="form-group">
                                <label class="sr-only">foto</label>
                                <tr bgcolor="skyblue">
                                    <td bgcolor="skyblue"><strong>Foto:</strong></td>
                                    <td><input type="file" name="foto" id="foto"></td>
                                </tr>
                            </div>
                            <div class="botones">
                                <div>
                                <a href="perfil.php"> 
                                    <button type="button" class="btn btn-primary">Omitir</button>
                                    </a></div>
                               <div>
                                   <input type="submit" class="btn btn-success"value="Confirmar"/></div>
                            </div>
                           
                        </form>
                    </div>
                </div>


              
            </div>

          
        </div>
    </div>
    
<script>
    var app = angular.module("myapp", []);
    app.controller("controlador", function($scope, $http) {


        $scope.init = function() {

            $scope.loadMascotasBarra = function() {

                $http({
                        method: "post",
                        url: "php/load_mascotas.php",
                        cache: "false",
                        dataType: "json",
                        data: $.param({
                            'iduser': <?php echo $_SESSION['iduser']; ?>
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    }).success(function(data) {

                        $scope.barramascotas = data;

                    })

                    .error(function(error, status) {
                        $scope.data.error = {
                            message: error,
                            status: status
                        };
                        console.log($scope.data.error.status);
                        alert($scope.data.error);

                    });

            }



            $scope.loadTipo = function() {
                $http.get("php/load_tipo.php")
                    .success(function(data) {
                        $scope.tipomasc2 = data;
                    })
            }
                
            $scope.loadRaza = function() {

                $http({
                        method: "post",
                        url: "php/load_raza.php",
                        cache: "false",
                        dataType: "json",
                        data: $.param({
                            'idtipo': $scope.tipomasc
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        }
                    }).success(function(data) {

                        $scope.raza2 = data;
                    })

                    .error(function(error, status) {
                        $scope.data.error = {
                            message: error,
                            status: status
                        };
                        console.log($scope.data.error.status);
                        alert($scope.data.error);
                        alert(error);
                        alert(status);
                    })

                    .then(function(xhr, textStatus) {
                        //alert(xhr.status);
                        //console.log(xhr.status);
                    });

            }

            $scope.loadMascotasBarra();
            $scope.loadTipo();
            $scope.loadRaza();
        }
    });

</script>
<script>
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        weekHeader: 'Sm',
        dateFormat: 'dd-mm-yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        maxDate: "0",
        showButtonPanel: true,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

</script>

</body>
</html>
